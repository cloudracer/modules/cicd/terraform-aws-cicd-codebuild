# Terraform CI/CD CodeBuild

## Overview

This Module creates an a Terraform Plan and a Terraform Apply CodeBUild Project.

## Usage

````
module "cicd_codebuild" {
  source  = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-cicd-codebuild.git?ref=master"

  codebuild_project_terraform_plan_name         = "${var.organisation}-landingzone-codebuild-plan-tflz"
  codebuild_project_terraform_plan_description  = "Terraform Plan CodeBuild Project"
  codebuild_project_terraform_apply_name        = "${var.organisation}-landingzone-codebuild-apply-tflz"
  codebuild_project_terraform_apply_description = "Terraform Apply CodeBuild Project"
  codebuild_terraform_version                   = "0.15.0"
  s3_logging_bucket_id                          = module.bootstrap.s3_logging_bucket_id
  codebuild_iam_role_arn                        = module.bootstrap.codebuild_iam_role_arn
  s3_logging_bucket                             = module.bootstrap.s3_logging_bucket

  tags                                          = local.tags
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_codebuild_project.codebuild_project_terraform_apply](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project) | resource |
| [aws_codebuild_project.codebuild_project_terraform_plan](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_codebuild_iam_role_arn"></a> [codebuild\_iam\_role\_arn](#input\_codebuild\_iam\_role\_arn) | ARN of the CodeBuild IAM role | `any` | n/a | yes |
| <a name="input_codebuild_project_terraform_apply_description"></a> [codebuild\_project\_terraform\_apply\_description](#input\_codebuild\_project\_terraform\_apply\_description) | Description for CodeBuild Terraform Apply Project | `any` | n/a | yes |
| <a name="input_codebuild_project_terraform_apply_name"></a> [codebuild\_project\_terraform\_apply\_name](#input\_codebuild\_project\_terraform\_apply\_name) | Name for CodeBuild Terraform Apply Project | `any` | n/a | yes |
| <a name="input_codebuild_project_terraform_plan_description"></a> [codebuild\_project\_terraform\_plan\_description](#input\_codebuild\_project\_terraform\_plan\_description) | Description for CodeBuild Terraform Plan Project | `any` | n/a | yes |
| <a name="input_codebuild_project_terraform_plan_name"></a> [codebuild\_project\_terraform\_plan\_name](#input\_codebuild\_project\_terraform\_plan\_name) | Name for CodeBuild Terraform Plan Project | `any` | n/a | yes |
| <a name="input_codebuild_terraform_version"></a> [codebuild\_terraform\_version](#input\_codebuild\_terraform\_version) | The Terraform Version needed for the CodeBuild Project | `string` | n/a | yes |
| <a name="input_s3_logging_bucket"></a> [s3\_logging\_bucket](#input\_s3\_logging\_bucket) | Name of the S3 bucket for access logging | `any` | n/a | yes |
| <a name="input_s3_logging_bucket_id"></a> [s3\_logging\_bucket\_id](#input\_s3\_logging\_bucket\_id) | ID of the S3 bucket for access logging | `any` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that should be applied to all Resources | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_codebuild_terraform_apply_name"></a> [codebuild\_terraform\_apply\_name](#output\_codebuild\_terraform\_apply\_name) | n/a |
| <a name="output_codebuild_terraform_plan_name"></a> [codebuild\_terraform\_plan\_name](#output\_codebuild\_terraform\_plan\_name) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
